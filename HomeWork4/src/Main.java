import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static boolean isPrime(int number) {
        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        if (isPrime(number)) {
            System.out.println("is prime");
        } else {
            System.out.println("ТУТ НЕ ПРОСТОЕ");
        }
    }
}
